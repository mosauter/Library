package de.Library.control.impl;

import java.util.LinkedList;

import com.google.inject.Inject;
import com.google.inject.Injector;

import de.Library.control.IMainControl;
import de.Library.model.IBook;

public class MainControl implements IMainControl {

    private final LinkedList<IBook> books;
    private final Injector injector;
    private final ReaderWriterControl rwc;

    @Inject
    public MainControl(final Injector injector) {
        this.injector = injector;
        rwc = injector.getInstance(ReaderWriterControl.class);
        books = rwc.readInBooks();
    }

    @Override
    public void addBook(final IBook book) {
        if (updateBook(book)) {
            return;
        }
        books.add(book);
    }

    private boolean updateBook(final IBook book) {
        for (final IBook stbook : books) {
            if (stbook.equals(book)) {
                stbook.setAuthor(book.getAuthor());
                stbook.setTitle(book.getTitle());
                stbook.setYear(book.getYear());
                return true;
            }
        }
        return false;
    }

    @Override
    public void addBook(final String isbn, final String title,
                        final String author, final int year) {
        this.addBook(createBook(isbn, title, author, year));
    }
    
    @Override
    public void updBook(IBook book) {
        this.addBook(book);
    }

    public IBook createBook(final String isbn, final String title,
                            final String author, final int year) {
        final IBook book = injector.getInstance(IBook.class);
        book.setISBN(isbn);
        book.setTitle(title);
        book.setAuthor(author);
        book.setYear(year);
        return book;
    }

    @Override
    public LinkedList<IBook> getAllBooks() {
        return books;
    }
    
    @Override
    public IBook getSingleBook(String isbn) {
        for (IBook b : books) {
            if (b.getISBN().equals(isbn)) {
                return b;
            }
        }
        return null;
    }

    @Override
    public void rateBook(final String isbn, final int rating) {
        for (final IBook book : books) {
            if (book.getISBN().equals(isbn)) {
                book.rate(rating);
                return;
            }
        }
    }

    @Override
    public void remBook(final String isbn) {
        for (final IBook book : books) {
            if (book.getISBN().equals(isbn)) {
                books.remove(book);
                return;
            }
        }
    }

    @Override
    public int borrow(final String isbn, final String owner) {
        for (final IBook book : books) {
            if (book.getISBN().equals(isbn)) {
                return borrowBook(book, owner);
            }
        }
        return 2;
    }

    private int borrowBook(final IBook book, final String owner) {
        return book.borrow(owner) ? 0 : 1;
    }

    @Override
    public void bringBack(final String isbn) {
        for (final IBook book : books) {
            if (book.getISBN().equals(isbn)) {
                book.bringBack();
                return;
            }
        }
    }
}
