package de.Library.control.impl;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import de.Library.model.IBook;
import de.Library.util.State;

public class ReaderWriterControl {

    private final MainControl mc;

    public ReaderWriterControl(final MainControl mc) {
        this.mc = mc;
    }

    public LinkedList<IBook> readInBooks() {
        final LinkedList<IBook> l = new LinkedList<>();
        final JSONParser parser = new JSONParser();
        try {
            final JSONArray array = (JSONArray) parser.parse(new FileReader(
                            "books.json"));
            for (int i = 0; i < array.size(); i++) {
                final JSONObject ob = (JSONObject) array.get(i);
                final IBook book = createEntireBook((String) ob.get("isbn"),
                                                    (String) ob.get("author"),
                                                    (String) ob.get("title"),
                                                    (String) ob.get("owner"),
                                                    (int) ob.get("year"),
                                                    State.valueOf((String) ob.get("state")),
                                                    IBook.DEFAULT_DATE_FORMAT.parse((String) ob.get("borrowDate")));
                l.add(book);
            }
        } catch (final Exception e) {

        }
        return l;
    }

    public IBook createEntireBook(final String isbn, final String author,
                                  final String title, final String owner,
                                  final int year, final State state,
                                  final Date date) {
        final IBook book = mc.createBook(isbn, title, author, year);
        book.setOwner(owner);
        book.setState(state);
        book.setBorrowDate(date);
        return book;
    }

    public void write() {
        final LinkedList<IBook> l = mc.getAllBooks();
        try (FileWriter fw = new FileWriter(new File("books.json"))) {
            final String jsonString = JSONValue.toJSONString(l);
            fw.write(jsonString);
            fw.flush();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
