package de.Library.control;

import java.util.LinkedList;

import de.Library.model.IBook;

public interface IMainControl {

    void addBook(IBook book);

    void addBook(String isbn, String title, String author, int year);
    
    void updBook(IBook book);

    LinkedList<IBook> getAllBooks();
    
    IBook getSingleBook(String isbn);

    void rateBook(String isbn, int rating);

    void remBook(String isbn);

    int borrow(String isbn, String owner);

    void bringBack(String isbn);
}
