package de.Library.rest;

import java.util.LinkedList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.inject.Inject;

import de.Library.control.IMainControl;
import de.Library.model.IBook;

@Path("/books")
public class RestService {
    
    private final IMainControl mc;
    private final Logger LOGGER = Logger.getLogger(RestService.class.getName());
    
    @Inject
    public RestService(IMainControl mc) {
        this.mc = mc;
    }
    
    @GET
    @Path("/get/all")
    @Produces(MediaType.APPLICATION_JSON)
    public LinkedList<IBook> getAllBooks() {
        LOGGER.debug("getAllBooks()");
        return this.mc.getAllBooks();
    }
    
    @GET
    @Path("/get/single")
    @Produces(MediaType.APPLICATION_JSON)
    public IBook getSingleBook(@QueryParam("isbn") String isbn) {
        LOGGER.debug("getSingleBook(): isbn = " + isbn);
        return mc.getSingleBook(isbn);
    }
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBook(IBook book) {
        LOGGER.debug("addBook(): book = " + book);
        mc.addBook(book);
        return Response.ok().build();
    }
    
    @POST
    @Path("/rate")
    public Response rateBook(@QueryParam("isbn") String isbn, @QueryParam("rating") int rating) {
        LOGGER.debug("rateBook(): ibsn = " + isbn);
        mc.rateBook(isbn, rating);
        return Response.ok().build();
    }
    
    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateBook(IBook book) {
        LOGGER.debug("updateBook(): book = " + book);
        return Response.ok().build();
    }
    
    @DELETE
    @Path("/delete")
    public Response rmBook(@QueryParam("isbn") String isbn) {
        LOGGER.debug("rmBook(): isbn = " + isbn);
        mc.remBook(isbn);
        return Response.ok().build();
    }
}
