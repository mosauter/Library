package de.Library.util;

/**
 * The different states of a book.
 *
 * @author Moritz Sauter <SauterMoritz at gmx dot de>
 */
public enum State {

    /**
     * Indicates that the book is borrowed.
     */
    BORROWED,
    /**
     * Indicates that the book is in the library.
     */
    AVAILABLE;
}
