package de.Library;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.Library.control.IMainControl;

public class Library {

    public static void main(final String[] args) {
        PropertyConfigurator.configure("log4j.properties");
        final Logger log = Logger.getLogger(Library.class.getName());
        final Injector in = Guice.createInjector(new LibraryModule());
        log.debug("Log something");
        final IMainControl mc = in.getInstance(IMainControl.class);
        mc.addBook("381948924", "Harry Potter", "JK");
        System.out.println(mc.getBooks());
    }
}
