package de.Library.model.impl;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import de.Library.model.IBook;
import de.Library.util.State;

@XmlRootElement(name = "Book")
public class Book implements IBook {

    @XmlAttribute(name = "isbn")
    private String isbn;
    @XmlAttribute(name = "title")
    private String title;
    @XmlAttribute(name = "author")
    private String author;
    @XmlAttribute(name = "year")
    private int year;
    @XmlAttribute(name = "owner")
    private String owner;
    @XmlAttribute(name = "borrowDate")
    private Date borrowDate;
    @XmlAttribute(name = "state")
    private State state;
    @XmlAttribute(name = "rating")
    private int rating;

    public Book() {
        this(null, null, null, 0, IBook.OWNER_LIBRARY,
             IBook.DEFAULT_BORROW_DATE, IBook.DEFAULT_STATE,
             IBook.DEFAULT_RATING);
    }

    public Book(final String isbn, final String title, final String author,
                final int year, final String owner, final Date borrowDate,
                final State state, final int rating) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.year = year;
        this.owner = owner;
        if (borrowDate == null) {
            this.borrowDate = IBook.DEFAULT_BORROW_DATE;
        } else {
            this.borrowDate = borrowDate;
        }
        this.state = state;
        this.rating = rating;
    }

    @Override
    public String getISBN() {
        return isbn;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public int getYear() {
        return year;
    }

    @Override
    public String getOwner() {
        return owner;
    }

    @Override
    public Date getBorrowDate() {
        return borrowDate;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void setISBN(final String isbn) {
        if (this.isbn == null) {
            this.isbn = isbn;
        }
    }

    @Override
    public void setTitle(final String title) {
        this.title = title;
    }

    @Override
    public void setAuthor(final String author) {
        this.author = author;
    }

    @Override
    public void setYear(final int year) {
        this.year = year;
    }

    @Override
    public void setOwner(final String owner) {
        this.owner = owner;
    }

    @Override
    public void setBorrowDate(final Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    @Override
    public void setState(final State newState) {
        state = newState;
    }

    @Override
    public void rate(final int rating) {
        this.rating = rating;
    }

    @Override
    public boolean borrow(final String newOwner) {
        if (state.equals(State.BORROWED)) {
            return false;
        }
        borrowDate = new Date();
        owner = newOwner;
        state = State.BORROWED;
        return true;
    }

    @Override
    public void bringBack() {
        owner = IBook.OWNER_LIBRARY;
        borrowDate = IBook.DEFAULT_BORROW_DATE;
        state = State.AVAILABLE;
    }

    @Override
    public String toSearchString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(isbn).append(" ");
        sb.append(title).append(" ");
        sb.append(author);
        if (state.equals(State.BORROWED)) {
            sb.append(" ").append(owner).append(" ");
            sb.append(IBook.DEFAULT_DATE_FORMAT.format(borrowDate));
        }
        return sb.toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Book) {
            final Book new_name = (Book) obj;
            return new_name.isbn.equals(isbn);
        }
        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{\"isbn\":\"").append(isbn).append("\",");
        sb.append("\"title\":\"").append(title).append("\",");
        sb.append("\"author\":\"").append(author).append("\",");
        sb.append("\"owner\":\"").append(owner).append("\",");
        sb.append("\"borrowDate\":\"")
        .append(IBook.DEFAULT_DATE_FORMAT.format(borrowDate)).append("\",");
        sb.append("\"state\":\"").append(state).append("\",");
        sb.append("\"rating\":\"").append(rating).append("\"}");
        return sb.toString();
    }
}
