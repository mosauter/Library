package de.Library.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.Library.util.State;

/**
 * Interface of a book.
 *
 * @author Moritz Sauter <SauterMoritz at gmx dot de>
 */
public interface IBook {

    /**
     * Default presentation of the owner when the book is
     * {@link de.Library.util.State#AVAILABLE}.
     */
    static final String OWNER_LIBRARY = "In Library";
    /**
     * Default presentation of the borrowDate when the book is
     * {@link de.Library.util.State#AVAILABLE}.
     */
    static final Date DEFAULT_BORROW_DATE = new Date(0);
    /**
     * Start value of the rating of a book.
     */
    static final int DEFAULT_RATING = 0;
    /**
     * Encapsulate the {@link de.Library.util.State#AVAILABLE} state.
     */
    static final State DEFAULT_STATE = State.AVAILABLE;
    /**
     * Indicates the default presentation of a date.
     */
    static final DateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(
                    "yyyy-MM-dd");

    /**
     * Getter for isbn.
     *
     * @return the isbn
     */
    String getISBN();

    /**
     * Getter for the title.
     *
     * @return the title
     */
    String getTitle();

    /**
     * Getter for the author.
     *
     * @return the author
     */
    String getAuthor();

    /**
     * Getter for the year.
     *
     * @return the year
     */
    int getYear();

    /**
     * Getter for the current owner of the book.
     * If the book is not {@link de.Library.util.State#BORROWED} it is the
     * default value represented by the constant {@link #OWNER_LIBRARY}.
     *
     * @return the current owner of the book
     */
    String getOwner();

    /**
     * Getter for the borrowDate. It represents the date on which an owner
     * borrowed the book.
     * if the book is not {@link de.Library.util.State#BORROWED} it is the
     * default value represented by the constant {@link #DEFAULT_BORROW_DATE}.
     *
     * @return the borrow date
     */
    Date getBorrowDate();

    /**
     * Getter for the current state of the book.
     *
     * @return either {@link de.Library.util.State#AVAILABLE} or
     *         {@link de.Library.util.State#BORROWED}
     */
    State getState();

    /**
     * Getter for the current rating of the book.
     *
     * @return the current rating
     */
    int getRating();

    /**
     * Setter for the isbn. Sets the isbn only if the isbn is not set before. To
     * change it after the creation you have to create another book.
     *
     * @param title
     *            the new isbn
     */
    void setISBN(String isbn);

    /**
     * Setter for the title.
     *
     * @param title
     *            the new title
     */
    void setTitle(String title);

    /**
     * Setter for the author.
     *
     * @param author
     *            the new author
     */
    void setAuthor(String author);

    /**
     * Setter for the year.
     * 
     * @param year
     *            the new year
     */
    void setYear(int year);

    /**
     * Setter for the owner.
     * Should only be used at the start of the application where books have to
     * be read from a file.
     * In all other cases you should use {@link #borrow(String)}.
     *
     * @param owner
     *            the new owner
     */
    void setOwner(String owner);

    /**
     * Setter for the borrow date.
     * Should only be used at the start of the application where books have to
     * be read from a file.
     * In all other cases you should use {@link #borrow(String)}.
     *
     * @param borrowDate
     *            the new borrow date
     */
    void setBorrowDate(Date borrowDate);

    /**
     * Setter for the state of the book.
     * Should only be used at the start of the application where books have to
     * be read from a file.
     * In all other cases you should either use {@link #borrow(String)} or
     * {@link #bringBack()}.
     *
     * @param newState
     *            the new state of the book
     */
    void setState(State newState);

    /**
     * Setter for the rating of the book.
     * Calculates average of the current rating and the new rating.
     *
     * @param rating
     *            a new rating
     */
    void rate(int rating);

    /**
     * Borrows a book.
     * Sets the current state to {@link de.Library.util.State#BORROWED} if the
     * book is not borrowed before.
     * Also sets the new owner to the newOwner and the borrowDate to the current
     * date.
     *
     * @param newOwner
     *            the new owner of the book if the book is successfully borrowed
     * @return true if the book is successfully borrowed, false if the book is
     *         already borrowed by another user
     */
    boolean borrow(String newOwner);

    /**
     * Brings the book back.
     * Sets the current state to {@link de.Library.util.State#AVAILABLE}.
     */
    void bringBack();

    /**
     * Combines the isbn, title, author and if the book is
     * {@link de.Library.util.State#BORROWED} it adds the current owner and
     * borrow date.
     *
     * @return the string for searching issues
     */
    String toSearchString();
}
