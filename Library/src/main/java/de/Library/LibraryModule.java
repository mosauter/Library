package de.Library;

import com.google.inject.AbstractModule;

import de.Library.control.IMainControl;
import de.Library.control.impl.MainControl;
import de.Library.model.IBook;
import de.Library.model.impl.Book;

public class LibraryModule extends AbstractModule {

    @Override
    protected void configure() {
        this.bind(IBook.class).to(Book.class);
        this.bind(IMainControl.class).to(MainControl.class);
    }
}
