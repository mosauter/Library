package de.Library.model.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BookTest {

    private Book b;

    @Before
    public void setUp() {
        this.b = new Book();
    }

    @Test
    public void testISBN() {
        Assert.assertTrue(this.b.getISBN() == null);
        final String isbn = "123";
        this.b.setISBN(isbn);
        Assert.assertEquals(isbn, this.b.getISBN());
        this.b.setISBN("456");
        Assert.assertEquals(isbn, this.b.getISBN());
    }

    @Test
    public void testAuthor() {
        Assert.assertTrue(this.b.getAuthor() == null);
        String author = "abc";
        this.b.setAuthor(author);
        Assert.assertEquals(author, this.b.getAuthor());
        author = "def";
        this.b.setAuthor(author);
        Assert.assertEquals(author, this.b.getAuthor());
    }

    @Test
    public void testTitle() {
        Assert.assertTrue(this.b.getTitle() == null);
        String title = "abc";
        this.b.setTitle(title);
        Assert.assertEquals(title, this.b.getTitle());
        title = "def";
        this.b.setTitle(title);
        Assert.assertEquals(title, this.b.getTitle());
    }

}
